let fact = document.querySelector("#fact");
let factText = document.querySelector("#factText");
let numberInput = document.querySelector("#numberInput");
let triviatype = 'number';
//numberInput.addEventListener('input', getFactAjax);
numberInput.addEventListener('input', getFactFetch);

let triviatypeform = document.querySelector("#triviatypeform");
triviatypeform.addEventListener('click', testForm);


/* Uses AJAX */
//function getFactAjax() {
//    let number = numberInput.value;
//
//    let xhr = new XMLHttpRequest();
//    xhr.open('GET', 'http://numbersapi.com/' + number);
//
//    xhr.onload = function () {
//        if (this.status == 200 && number != '') {
//            fact.style.display = 'block';
//            factText.innerText = this.responseText;
//        } else if (number == '') {
//            fact.style.display = 'none';
//            factText.innerText = '';
//        }
//    }
//
//    xhr.send();
//}

/* Uses Fetch */
function getFactFetch(event) {
    let number = numberInput.value;
    let request = "http://numbersapi.com/";

    if (triviatype == 'number') {
        request += number;
    } else if (triviatype == 'math') {
        request += number + '/math';
    } else if (triviatype == 'date' && event.key == 'Enter') {
        let numberdate = numberInput.valueAsDate;
        number = (numberdate.getMonth() + 1) + '/' + (numberdate.getDate() + 1);
        request += number + "/date";
    }

    //    console.log(request);
    if (number != '') {
        if (triviatype == 'date' && event.key != 'Enter')
            return;
        fact.style.display = 'block';
        fetch(request)
            .then(response => response.text())
            .then(data => {
                factText.innerText = data;
            })
            .catch(err => console.log(err));
    } else {
        fact.style.display = 'none';
        factText.innerText = '';
    }
}

function testForm() {
    let header = document.querySelector("#header");
    let subheader = document.querySelector("#subheader");

    triviatype = document.forms["triviatypeform"]["triviatype"].value;
    if (triviatype == 'number') {
        numberInput.removeEventListener('keypress', getFactFetch);
        numberInput.addEventListener('input', getFactFetch)
        header.innerText = "Number Facts";
        subheader.innerText = "Enter a number and get a fact";
        numberInput.type = "number";
        numberInput.placeholder = "Enter any number...";
    } else if (triviatype == 'math') {
        numberInput.removeEventListener('keypress', getFactFetch);
        numberInput.addEventListener('input', getFactFetch)
        header.innerText = "Math Facts";
        subheader.innerText = "Enter a number and get a math fact";
        numberInput.type = "number";
        numberInput.placeholder = "Enter any number...";
    } else if (triviatype == 'date') {
        numberInput.removeEventListener('input', getFactFetch);
        numberInput.addEventListener('keypress', getFactFetch)
        header.innerText = "Date Facts";
        subheader.innerText = "Enter a date and get a fact";
        numberInput.type = "date";
    }
}
